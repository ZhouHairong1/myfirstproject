package Test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @program: com.SpringBootDemo
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-03-20 16:011
 **/
@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory factory){

        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();

        template.setConnectionFactory(factory);

//        template.setDefaultSerializer(new JdkSerializationRedisSerializer());
        template.setKeySerializer(RedisSerializer.string());
        template.setValueSerializer(new JdkSerializationRedisSerializer());
        template.setHashKeySerializer(RedisSerializer.string());
        template.setHashValueSerializer(new JdkSerializationRedisSerializer());

        return template;
    }
}