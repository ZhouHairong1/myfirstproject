package Test;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;

/**
 * @program: com.SpringBootDemo
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-03-20 14:511
 **/
@SpringBootTest
@SpringBootApplication
public class RedisTest {

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Test
    public void Test(){
        if (redisTemplate==null){
            System.out.println("null");
        }else {
            System.out.println("not nul");
        }

        redisTemplate.opsForValue().set("k1",new Integer(1));
        System.out.println(redisTemplate.opsForValue().get("k1"));
        redisTemplate.opsForHash().put("k2","1k","1v");
        System.out.println(redisTemplate.opsForHash().get("k2", "1k"));
        stringRedisTemplate.opsForValue().get("k1");
    }

    public static void main(String[] args) {
        SpringApplication.run(RedisTest.class,args);
    }


}