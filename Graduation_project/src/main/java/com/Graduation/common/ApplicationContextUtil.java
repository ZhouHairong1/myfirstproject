package com.Graduation.common;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-07 19:58
 **/
public class ApplicationContextUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext)throws BeansException {
        if (ApplicationContextUtil.applicationContext==null){
            ApplicationContextUtil.applicationContext=applicationContext;
        }
    }

    //通过name获取bean对象
    public static Object getBean(String name){
        return applicationContext.getBean(name);
    }

    public static<T> T getBean(String name, Class<T> clazz){
        return applicationContext.getBean(name,clazz);
    }

}