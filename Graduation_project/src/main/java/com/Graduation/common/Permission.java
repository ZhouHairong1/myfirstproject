package com.Graduation.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Permission {
    enum UserType{
        //游客权限
        TOURIST(1),
        //普通用户权限
        USER(2),
        //超级管理员权限
        ADMIN(3);
        UserType(int i) {
            value=i;
        }

        public int getPermission(){
            return value;
        }

        int value=0;

    }

    UserType value() default UserType.TOURIST;

}