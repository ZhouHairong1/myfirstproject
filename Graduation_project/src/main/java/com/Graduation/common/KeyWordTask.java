package com.Graduation.common;

import com.Graduation.Utils.Log;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Map;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-05-10 13:04
 **/
public class KeyWordTask implements Runnable {
    private static final Logger log= Log.rootLogger;
    private RedisTemplate redisTemplate;
    public KeyWordTask() {
        this.redisTemplate=  ApplicationContextUtil.getBean("redisTemplate",RedisTemplate.class);
    }

    @Override
    public void run() {
        log.info("开启用户历史语料库计算线程");
        try {
            //遍历用户最近访问时间
            Map map=redisTemplate.opsForHash().entries(Constants.LAST_HANDLE_TIME);
            for (Object entry1 : map.entrySet()) {
                Map.Entry entry=(Map.Entry)entry1;
                Object uid=entry.getKey();
                long lastHandleTime=Long.valueOf(String.valueOf(entry.getValue()));
                if (System.currentTimeMillis()-lastHandleTime>Constants.LOGIN_TIMEOUT){
                    //用户登录超时删除token
                    Object token=redisTemplate.opsForHash().get(Constants.AUTHENTICATE_TOKEN,uid);
                    if (!StringUtils.isBlank(String.valueOf(token))){
                        redisTemplate.opsForHash().delete(Constants.AUTHENTICATE_TOKEN,uid);
                        redisTemplate.opsForHash().delete(Constants.LAST_HANDLE_TIME,uid);
                    }
                }
            }
        }catch (Exception e){
            log.error("用户历史语料库计算线程线程异常",e);
        }
        log.info("用户历史语料库计算线程执行成功");
    }

}