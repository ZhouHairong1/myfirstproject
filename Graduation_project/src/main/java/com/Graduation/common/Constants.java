package com.Graduation.common;

public interface Constants {

    //Redis中token的hash名
    String AUTHENTICATE_TOKEN ="GP_AUTHENTICATE_TOKEN";

    //Redis中token时间的hash名
    String LAST_HANDLE_TIME ="GP_LAST_HANDLE_TIME";

    //公私钥的hash名
    String LOGIN_PRIVATE_KEY="GP_LOGIN_PRIVATE_KEY";
    String LOGIN_PUBLIC_KEY="GP_LOGIN_PUBLIC_KEY";

    //token超时时间
    long LOGIN_TIMEOUT =600*1000L;

}