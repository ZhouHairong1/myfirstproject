package com.Graduation.common;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-07 19:57
 **/
@Component
public class AccessInterceptor implements HandlerInterceptor {

    @Autowired
    RedisTemplate redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)throws Exception {
        System.out.println("拦截地址："+request.getRequestURL().toString());
        int level=0;
        String token=request.getHeader("Authenticate_Token");
        String username= request.getHeader("username");
        //1.如果token过期或不存在
        String realToken=String.valueOf(redisTemplate.opsForHash().get(Constants.AUTHENTICATE_TOKEN,username));
        if (token==null||!StringUtils.equals(realToken,token)){
            level=1;
        }else {
            if(username.equals("admin")){
                level=3;
            }else {
                level=2;
            }
        }
        Permission annotation;
        int permission;
        HandlerMethod handlerMethod;
        if (handler instanceof HandlerMethod){
            handlerMethod=(HandlerMethod)handler;
        }else {
            return false;
        }

        //获取被访问方法的访问权限，如果被访问方法未被注解修饰直接放行
        annotation=handlerMethod.getMethodAnnotation(Permission.class);
        if (annotation==null){
            return true;
        }else {
            permission=annotation.value().getPermission();
        }

        //如果访问权限大于等于被访问方法的访问权限
        if (level>=permission){
            return true;
        }else {
            response.sendRedirect("/gp_system/tourist/login");
            return false;
        }
    }

}