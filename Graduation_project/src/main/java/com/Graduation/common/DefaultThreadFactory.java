package com.Graduation.common;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-07 20:43
 **/
class DefaultThreadFactory implements ThreadFactory {
    private final String namePrefix;
    private final AtomicInteger nextId=new AtomicInteger(1);
    public DefaultThreadFactory(String namePrefix) {
        this.namePrefix = namePrefix;
    }

    @Override
    public Thread newThread(Runnable task) {
        String name=namePrefix+",编号:"+nextId.getAndIncrement();
        return new Thread(task,name);
    }

}