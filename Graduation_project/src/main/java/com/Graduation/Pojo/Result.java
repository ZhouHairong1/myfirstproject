package com.Graduation.Pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-03-30 20:28
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result implements Serializable {

    private String statusCode;
    private String statusString;
    private String requestURL;
    private Object data;

    public Result(String statusCode, String statusString, String requestURL) {
        this.statusCode = statusCode;
        this.statusString = statusString;
        this.requestURL = requestURL;
    }

    public Result(String statusCode, String statusString) {
        this.statusCode=statusCode;
        this.statusString=statusString;
    }

    public Result(Object data) {
        this.statusCode = "200";
        this.statusString = "success";
        this.data = data;
    }
}