package com.Graduation.Pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-05-09 11:35
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ESDocument {

    public String title;
    public String content;

}