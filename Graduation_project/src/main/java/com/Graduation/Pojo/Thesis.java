package com.Graduation.Pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-03-29 22:17
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Thesis {
    public int thesis_id;
    public int user_id;
    public String thesis_name;
    public int thesis_length;
    public int thesis_star;
    public int download_num;
    public String upload_time;
    public String fileType;

    public Thesis(int user_id, String thesis_name, int thesis_length, String upload_time,String fileType) {
        this.user_id = user_id;
        this.thesis_name = thesis_name;
        this.thesis_length = thesis_length;
        this.upload_time = upload_time;
        this.fileType = fileType;
    }
}