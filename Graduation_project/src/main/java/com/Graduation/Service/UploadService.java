package com.Graduation.Service;

import com.Graduation.Dao.ESDao;
import com.Graduation.Dao.FTPDao;
import com.Graduation.Dao.Mappers.ThesisDao;
import com.Graduation.Dao.Mappers.UserDao;
import com.Graduation.Exception.MainException;
import com.Graduation.Pojo.Thesis;
import com.Graduation.Utils.DateUtil;
import com.Graduation.Utils.DocUtil;
import com.Graduation.Utils.Log;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-01 21:51
 **/
@Service

public class UploadService {

    @Autowired
    ESDao esDao;

    @Autowired
    FTPDao ftpDao;

    @Autowired
    ThesisDao thesisDao;

    @Autowired
    UserDao userDao;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    private static final Logger log= Log.rootLogger;
    public void uploadDocFiles(MultipartFile[] files,String username){
        for (MultipartFile file : files) {
            uploadDocFile(file,username);
        }
    }

    public void uploadDocFile(MultipartFile file,String username){
        Thesis thesis;
        int thesis_id;
        String thesis_name;
        String text;
        int thesis_length;
        int user_id;
        String fileType;
        try {
            thesis_name=file.getOriginalFilename();
            //临时thesis对象判断数据库中是否已存在同名论文
            Thesis tmpThesis=new Thesis();
            tmpThesis.setThesis_name(thesis_name);
            if (thesisDao.selectThesis(tmpThesis)!=null){
                throw new MainException.FTPUploadException("Mysql表中存在该同名论文");
            }
            if (thesis_name.contains(".docx")){
                text= DocUtil.readDocxFileAndGetText(file.getInputStream());
                fileType=".docx";
            } else if (thesis_name.contains(".doc")) {
                text= DocUtil.readDocFileAndGetText(file.getInputStream());
                fileType=".doc";
            }else {
                throw new MainException.FileTypeException();
            }
            //根据用户名获取用户id
            user_id=userDao.selectUserIdByName(username);
            //以字符串形式获取当前时间
            String nowTime=DateUtil.getNowTime();
            //获取论文中文字的长度
            thesis_length=text.length();
            //删除文件后缀
            thesis_name=DocUtil.getFileName(thesis_name);
            //创建论文对象插入数据库
            thesis=new Thesis(user_id,thesis_name,thesis_length,nowTime,fileType);
            //向数据库中插入论文信息并获取返回的论文id
            thesis_id=thesisDao.insertThesis(thesis);
            //ftp以流上传文件
            ftpDao.uploadFTP(thesis_id+fileType,file.getInputStream());
            //ES存储文件的Text及文件在ftp中的存储路径
//            esDao.storeThesis(thesis);
        }catch (Exception e) {
            if (e instanceof MainException){
                throw (MainException) e;
            }else {
                throw new MainException.FTPUploadException("文件上传操作失败");
            }
        }
    }

}