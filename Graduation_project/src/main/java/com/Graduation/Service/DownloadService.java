package com.Graduation.Service;

import com.Graduation.Dao.ESDao;
import com.Graduation.Dao.FTPDao;
import com.Graduation.Dao.Mappers.ThesisDao;
import com.Graduation.Exception.MainException;
import com.Graduation.Pojo.Thesis;
import com.Graduation.Utils.DocUtil;
import com.Graduation.Utils.Log;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-02 21:32
 **/
@Service

public class DownloadService {

    @Autowired
    ESDao esDao;

    @Autowired
    FTPDao ftpDao;

    @Autowired
    ThesisDao thesisDao;

    private static final Logger log= Log.rootLogger;

    public void downloadByName(String thesis_name) throws UnsupportedEncodingException {
        String fileId=null;
        String fileType=null;
        Thesis selectThesis;
        Thesis tmpThesis=new Thesis();
        tmpThesis.setThesis_name(thesis_name);
        if ((selectThesis=thesisDao.selectThesis(tmpThesis))==null){
            throw new MainException.MysqlSearchException("Mysql中不存在该论文注册信息");
        }else {
            fileId=String.valueOf(selectThesis.getThesis_id());
            fileType= String.valueOf(selectThesis.getFileType());
            HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
            response.setContentType("application/force-download");
            response.addHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(fileId+fileType, "UTF-8"));
            ftpDao.downloadFile(fileId+fileType, response);
        }
    }

}