package com.Graduation.Service;

import com.Graduation.Dao.ESDao;
import com.Graduation.Dao.FTPDao;
import com.Graduation.Exception.MainException;
import com.Graduation.Utils.Log;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-02 21:34
 **/
@Service
public class SearchService {

    @Autowired
    ESDao esDao;

    @Autowired
    FTPDao ftpDao;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    private static final Logger log= Log.rootLogger;

    public Object searchThesis(String word){
        try {
            return esDao.searchThesis(word);
        }catch (IOException e) {
            log.error("执行关键词查询操作失败",e);
            throw new MainException.ESSearchException("执行关键词查询操作失败");
        }
    }

    public Object searchThesis_smart(String word,int userId){
        try {
            return esDao.searchThesis_smart(word,userId);
        }catch (Exception e) {
            log.error("执行关键词查询操作失败",e);
            throw new MainException.ESSearchException("执行关键词查询操作失败");
        }
    }

}