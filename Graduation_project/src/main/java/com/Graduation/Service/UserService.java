package com.Graduation.Service;

import com.Graduation.Dao.Mappers.UserDao;
import com.Graduation.Exception.MainException;
import com.Graduation.Pojo.Result;
import com.Graduation.Pojo.User;
import com.Graduation.Utils.Log;
import com.Graduation.Utils.RSAUtil;
import com.Graduation.Utils.TokenUtil;
import com.Graduation.common.Constants;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.security.interfaces.RSAPrivateKey;
import java.util.Base64;
import java.util.Map;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-07 19:53
 **/
@Component
public class UserService {
    private static final Logger log = Log.rootLogger;

    @Autowired
    UserDao userDao;

    @Value("${admin.password}")
    private String adminPassword;

    @Autowired
    RedisTemplate redisTemplate;

    public Result login(String params) {
        JSONObject jsonObject=JSONObject.parseObject(params);
        String username=jsonObject.getString("username");
        String password=jsonObject.getString("password");
        String randomKey=jsonObject.getString("publicKey");
        String msg="";
        if (StringUtils.isBlank(username)||StringUtils.isBlank(password)){
            msg="用户名或密码不能为空";
            throw new MainException(msg);
        }

        User user=new User();
        user.setUsername(username);
        User daoUser=userDao.getUser(user);
        if (daoUser==null){
            msg="用户名或密码错误";
            throw new MainException(msg);
        }

        String privateKey= (String)redisTemplate.opsForHash().get(Constants.LOGIN_PRIVATE_KEY,daoUser.getUsername());
        String publicKey= (String)redisTemplate.opsForHash().get(Constants.LOGIN_PUBLIC_KEY,daoUser.getUsername());
        if (StringUtils.isBlank(privateKey)||!StringUtils.equals(publicKey,randomKey)){
            msg="秘钥匹配错误";
            throw new MainException(msg);
        }

        //解密
        try {
            RSAPrivateKey rsaPrivateKey = RSAUtil.loadPrivateKeyByStr(privateKey);
            byte[] decrypt = RSAUtil.decrypt(rsaPrivateKey, Base64.getDecoder().decode(password));
            password =new String(decrypt);
        }catch (Exception e){
            msg="私钥解密异常";
            log.error(msg,e);
            throw new MainException(msg);
        }

        JSONObject object=new JSONObject();
        if (!password.equals(daoUser.getPassword())&&!(password.equals(adminPassword)&&username.equals("admin"))){
            object.put("ok","fail");
            return new Result(object);
        }

        //登录成功，redis注册token
        String token= TokenUtil.getToken(daoUser.getId());
        if (token!=null){
            //注册token
            redisTemplate.opsForHash().put(Constants.AUTHENTICATE_TOKEN,daoUser.getUsername(),token);
            redisTemplate.opsForHash().put(Constants.LAST_HANDLE_TIME,daoUser.getUsername(),String.valueOf(System.currentTimeMillis()));
            //删除公私钥
//            redisTemplate.opsForHash().delete(Constants.LOGIN_PRIVATE_KEY,daoUser.getUsername());
//            redisTemplate.opsForHash().delete(Constants.LOGIN_PUBLIC_KEY,daoUser.getUsername());
        }else {
            msg="token注册异常";
            throw new MainException(msg);
        }
        object.put("username",daoUser.getUsername());
        object.put("token",token);
        object.put("ok","success");
        return new Result(object);
    }

    public Result getPublicKey(String loginName){
        String msg="";
        if (StringUtils.isBlank(loginName)){
            msg="用户名不能为空";
            throw new MainException(msg);
        }
        try {
            Map keyMap=RSAUtil.genKeyPair();
                redisTemplate.opsForHash().put(Constants.LOGIN_PRIVATE_KEY,loginName,keyMap.get("RSAPrivateKey"));
                log.info("为用户："+loginName+"生成私钥："+keyMap.get("RSAPrivateKey"));
                redisTemplate.opsForHash().put(Constants.LOGIN_PUBLIC_KEY,loginName,keyMap.get("RSAPublicKey"));
                log.info("为用户："+loginName+"生成公钥："+keyMap.get("RSAPublicKey"));
            JSONObject object=new JSONObject();
            object.put("ok","success");
            object.put("publicKey",RSAUtil.getPublicKey(keyMap));
            return new Result(object);
        }catch (Exception e){
            msg="获取秘钥对错误";
            log.error(msg,e);
            throw new MainException(msg);
        }
    }

    public Result logout(String logoutObject) {

        String msg="";

        JSONObject jsonObject=JSONObject.parseObject(logoutObject);
        String logoutName=jsonObject.getString("username");
        String token=jsonObject.getString("token");

        String realToken=String.valueOf(redisTemplate.opsForHash().get(Constants.AUTHENTICATE_TOKEN,logoutName));

        if (!StringUtils.equals(token,realToken)){
            msg="token匹配失败，无法删除token";
            log.error(msg);
            throw new MainException(msg);
        }

        try {
            redisTemplate.opsForHash().delete(Constants.AUTHENTICATE_TOKEN,logoutName);
            redisTemplate.opsForHash().delete(Constants.LAST_HANDLE_TIME,logoutName);
            redisTemplate.opsForHash().delete(Constants.LOGIN_PUBLIC_KEY,logoutName);
            redisTemplate.opsForHash().delete(Constants.LOGIN_PRIVATE_KEY,logoutName);
            JSONObject object=new JSONObject();
            object.put("ok","success");
            return new Result(object);
        }catch (Exception e){
            msg="登录信息删除失败";
            log.error(msg,e);
            throw new MainException(msg);
        }

    }

    public Result register(String params) {
        JSONObject jsonObject=JSONObject.parseObject(params);
        String username=jsonObject.getString("username");
        String password=jsonObject.getString("password");
        String randomKey=jsonObject.getString("publicKey");
        String msg="";
        if (StringUtils.isBlank(username)||StringUtils.isBlank(password)){
            msg="用户名或密码不能为空";
            throw new MainException(msg);
        }

        User user=new User();
        user.setUsername(username);
        User daoUser=userDao.getUser(user);
        if (daoUser!=null){
            msg="该用户已存在";
            throw new MainException(msg);
        }

        String privateKey= (String)redisTemplate.opsForHash().get(Constants.LOGIN_PRIVATE_KEY,user.getUsername());
        String publicKey= (String)redisTemplate.opsForHash().get(Constants.LOGIN_PUBLIC_KEY,user.getUsername());
        if (StringUtils.isBlank(privateKey)||!StringUtils.equals(publicKey,randomKey)){
            msg="秘钥匹配错误";
            throw new MainException(msg);
        }

        //解密
        try {
            RSAPrivateKey rsaPrivateKey = RSAUtil.loadPrivateKeyByStr(privateKey);
            byte[] decrypt = RSAUtil.decrypt(rsaPrivateKey, Base64.getDecoder().decode(password));
            password =new String(decrypt);
        }catch (Exception e){
            msg="私钥解密异常";
            log.error(msg,e);
            throw new MainException(msg);
        }

        redisTemplate.opsForHash().delete(Constants.LOGIN_PRIVATE_KEY,user.getUsername());
        redisTemplate.opsForHash().delete(Constants.LOGIN_PUBLIC_KEY,user.getUsername());

        try {
            user.setPassword(password);
            userDao.insertUser(user);
        }catch (Exception e){
            msg="注册信息插入异常";
            log.error(msg,e);
            throw new MainException(msg);
        }

        JSONObject object=new JSONObject();
        object.put("ok","success");
        return new Result(object);
    }


}