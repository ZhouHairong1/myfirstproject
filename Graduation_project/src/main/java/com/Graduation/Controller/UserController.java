package com.Graduation.Controller;

import com.Graduation.Exception.MainException;
import com.Graduation.Pojo.Result;
import com.Graduation.Service.DownloadService;
import com.Graduation.Service.SearchService;
import com.Graduation.Service.UploadService;
import com.Graduation.Service.UserService;
import com.Graduation.Utils.Log;
import com.Graduation.common.Permission;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Headers;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-02 21:39
 **/
@RestController

@RequestMapping("/gp_system/user")
public class UserController {
    private static final Logger log= Log.rootLogger;

    @Autowired
    UploadService uploadService;

    @Autowired
    DownloadService downloadService;

    @Autowired
    UserService userService;

    @PostMapping("/upload")
    @Permission(value = Permission.UserType.USER)
    public Result upload(MultipartFile file, @RequestHeader(value = "username") String username) {
        log.info("开始执行Doc上传接口");
        try {
                if (file.isEmpty()||!StringUtils.isNotBlank(file.getOriginalFilename())) {
                    throw new MainException.FTPUploadException("上传文件内容或文件名无效");
            }
            uploadService.uploadDocFile(file,username);
            log.info("执行Doc上传接口成功");
        }catch (Exception e) {
            log.error("执行Doc上传接口失败",e);
            if (e instanceof MainException) {
                throw (MainException) e;
            }else {
                throw new MainException("Doc上传接口内部错误", e.getMessage());
            }
        }
        return new Result("200","success");
    }

    @RequestMapping("/download")
    @Permission(value =Permission.UserType.USER)
    public Result download(@RequestParam("name") String thesis_name){
        log.info("开始执行Doc下载接口");
        if (!StringUtils.isNotBlank(thesis_name)){
            throw new MainException.FTPDownloadException("下载文件名无效");
        }
        try {
            downloadService.downloadByName(thesis_name);
            log.info("开始执行Doc下载成功");
            return new Result("200","success");
        }catch (Exception e) {
            log.error("开始执行Doc下载失败",e);
            if (e instanceof MainException){
                throw  (MainException)e;
            }else {
                throw new MainException.FTPDownloadException();
            }
        }
    }

    @RequestMapping("/logout")
    @Permission(value =Permission.UserType.USER)
    public Result logout(@RequestBody String logoutObject){
        log.info("开始执行登出接口[POST /gp_system/tourist/logout]");
        try{
            return userService.logout(logoutObject);
        }catch (Exception e){
            log.error("执行登出接口异常",e);
            if (e instanceof MainException){
                throw (MainException)e;
            }else {
                throw new MainException("登出接口执行失败");
            }
        }
    }

    @RequestMapping("/test1/{number}")
    @Permission(value =Permission.UserType.USER)
    public String testLevel1(@PathVariable int number){
        return String.valueOf(number);
    }

    @RequestMapping("/test2/{number}")
    @Permission(value =Permission.UserType.ADMIN)
    public String testLevel2(@PathVariable int number){
        return String.valueOf(number);
    }

    @RequestMapping("/test3/{number}")
    @Permission(value =Permission.UserType.TOURIST)
    public String testLevel3(@PathVariable int number){
        return String.valueOf(number);
    }

}