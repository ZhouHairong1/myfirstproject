package com.Graduation.Controller;

import com.Graduation.Exception.MainException;
import com.Graduation.Pojo.Result;
import com.Graduation.Service.SearchService;
import com.Graduation.Service.UserService;
import com.Graduation.Utils.Log;
import com.Graduation.common.Permission;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-07 19:49
 **/
@RestController
@RequestMapping(value = "/gp_system/tourist",produces = {"application/json;charset=UTF-8"})
public class TouristController {

    private static final Logger log = Log.rootLogger;

    @Autowired
    UserService userService;

    @Autowired
    SearchService searchService;

    @RequestMapping("/searchThesisByMatch")
    @Permission(value =Permission.UserType.TOURIST)
    public Result searchThesisByMatch(@RequestParam("word") String word){
        log.info("开始执行论文查找接口");
        Object data=null;
        try {
            data=searchService.searchThesis(word);
            if (data==null){
                throw new MainException.ESSearchException("查找接口返回数据错误！");
            }
            log.info("执行论文查找接口成功");
        }catch (Exception e) {
            if (e instanceof MainException){
                throw (MainException) e;
            }else {
                log.error(e.getMessage(),e);
                throw new MainException.ESSearchException("执行论文查找接口失败");
            }
        }
        return new Result(data);
    }

    @RequestMapping("/searchThesisByMatch_smart")
    @Permission(value =Permission.UserType.TOURIST)
    public Result searchThesisByMatch_smart(@RequestParam("word") String word){
        log.info("开始执行论文查找接口");
        Object data=null;
        try {
            data=searchService.searchThesis_smart(word,1);
            if (data==null){
                throw new MainException.ESSearchException("查找接口返回数据错误！");
            }
            log.info("执行论文查找接口成功");
        }catch (Exception e) {
            if (e instanceof MainException){
                throw (MainException) e;
            }else {
                log.error(e.getMessage(),e);
                throw new MainException.ESSearchException("执行论文查找接口失败");
            }
        }
        return new Result(data);
    }

    @RequestMapping("/login")
    @Permission(value =Permission.UserType.TOURIST)
    public Result login(@RequestBody String params){
        log.info("开始执行登录接口[POST /gp_system/tourist/login]");
        try{
            return userService.login(params);
        }catch (Exception e){
            log.error("执行登录接口异常",e);
            if (e instanceof MainException){
                throw (MainException)e;
            }else {
                throw new MainException("登录接口执行失败");
            }
        }
    }

    @RequestMapping("/publicKey/{loginName}")
    @Permission(value =Permission.UserType.TOURIST)
    public Result getPublicKey(@PathVariable String loginName){
        log.info("开始执行公钥获取接口[POST /gp_system/tourist/publicKey]");
        try{
            return userService.getPublicKey(loginName);
        }catch (Exception e){
            log.error("执行公钥获取接口异常",e);
            if (e instanceof MainException){
                throw (MainException)e;
            }else {
                throw new MainException("公钥获取接口执行失败");
            }
        }
    }

    @RequestMapping("/register")
    @Permission(value = Permission.UserType.TOURIST)
    public Result register(@RequestBody String params){
        log.info("开始执行注册接口[POST /gp_system/tourist/register]");
        try{
            return userService.register(params);
        }catch (Exception e){
            log.error("执行注册接口异常",e);
            if (e instanceof MainException){
                throw (MainException)e;
            }else {
                throw new MainException("注册接口执行失败");
            }
        }
    }

}