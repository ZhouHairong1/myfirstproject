package com.Graduation.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-03 11:05
 **/
public class DateUtil {
    public static String getNowTime(){
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }
}