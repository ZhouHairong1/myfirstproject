package com.Graduation.Utils;

import com.Graduation.Exception.MainException;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFPictureData;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.List;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-01 22:08
 **/
public class DocUtil {
    public static String readDocFileAndGetText(InputStream inputStream) {
        try {
            HWPFDocument document=new HWPFDocument(inputStream);
            return document.getText().toString();
        } catch (IOException e) {
            throw new MainException.DocReadException();
        }
    }

    public static String readDocxFileAndGetText(InputStream inputStream) {
        try {
            XWPFDocument document=new XWPFDocument(inputStream);
            XWPFWordExtractor wordExtractor=new XWPFWordExtractor(document);
            return wordExtractor.getText();
        } catch (IOException e) {
            throw new MainException.DocReadException();
        }
    }

    public static String getFileName(String name){
        String[] words=name.split("\\.");
        return words[0];
    }

    public static String getFileType(String name){
        String[] words=name.split("\\.");
        return "."+words[1];
    }

}