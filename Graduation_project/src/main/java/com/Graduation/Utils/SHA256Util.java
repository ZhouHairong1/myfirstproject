package com.Graduation.Utils;

import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-07 20:01
 **/
public class SHA256Util {
    public static String getSHA256(String str)throws NoSuchAlgorithmException {
        MessageDigest messageDigest=MessageDigest.getInstance("SHA-256");
        String signature= Hex.encodeHexString(messageDigest.digest(str.getBytes()));
        return signature;
    }

}