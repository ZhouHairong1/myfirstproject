package com.Graduation.Utils;

import com.alibaba.fastjson.JSONObject;

import java.security.NoSuchAlgorithmException;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-07 20:01
 **/
public class TokenUtil {

    //按照用户id和当前时间生成token
    public static String getToken(int uid) {
        try {
            JSONObject headerObject=new JSONObject();
            headerObject.put("uid",uid);
            headerObject.put("currentTime",System.currentTimeMillis());
            return SHA256Util.getSHA256(headerObject.toString());
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}