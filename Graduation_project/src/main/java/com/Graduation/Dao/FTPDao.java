package com.Graduation.Dao;

import com.Graduation.Config.FTPConfig;
import com.Graduation.Exception.MainException;
import com.Graduation.Utils.Log;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-01 21:50
 **/
@Repository
public class FTPDao {

    @Autowired
    FTPConfig ftpConfig;

    private static final Logger log= Log.rootLogger;

    public void uploadFTP(String fileName, InputStream inputStream)throws IOException {
        log.info("开始FTP文件上传");
        FTPClient ftpClient=null;
        String uploadFileName=ftpConfig.uploadPath+"/"+fileName;
        try {
            ftpClient=getFTPConnection();
            if(!isExistDir(ftpClient,ftpConfig.uploadPath)){
                throw new MainException.FTPUploadException();
            }
            if (isExistFile(ftpClient,uploadFileName)){
                throw new MainException.FTPUploadException();
            }
            if (!ftpClient.storeFile(uploadFileName,inputStream)){
                throw new MainException.FTPUploadException();
            }else {
                log.info("FTP文件上传成功");
            }
        }catch (Exception e) {
            log.error("FTP文件上传失败");
            throw e;
        }finally {
            closeConnection(ftpClient,inputStream);
        }
    }


    //根据FTPConfig配置信息获取FTP连接
    public FTPClient getFTPConnection(){
        FTPClient ftpClient=null;
        try {
            ftpClient=new FTPClient();
            ftpClient.setConnectTimeout(30*1000);
            ftpClient.connect(ftpConfig.host,ftpConfig.port);
            ftpClient.login(ftpConfig.username,ftpConfig.password);
            ftpClient.setControlEncoding("UTF-8");
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            //通知ftp服务器开启一个端口接受数据(linux 环境下可能需要该设置)
            ftpClient.enterLocalPassiveMode();
            if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())){
                ftpClient.disconnect();
                log.error("FTP连接失败");
                throw new MainException.FTPConnectionException();
            }else {
                log.error("FTP连接成功");
                return ftpClient;
            }
        }catch (Exception e) {
            log.error("FTP连接失败");
            throw new MainException.FTPConnectionException();
        }
    }

    //判断指定传输路径是否存在，是的话跳转到对应工作路径返回true，不是的话返回false
    public boolean isExistDir(FTPClient ftpClient,String path)throws IOException {
        boolean flag;
        if (ftpClient.changeWorkingDirectory(path)){
            flag=true;
        }else {
            log.info("传输路径不存在");
            flag=false;
        }
        return flag;
    }

    //判断指定文件名文件是否存在
    public boolean isExistFile(FTPClient ftpClient,String uploadFileName)throws IOException {
        boolean flag;
        String filePath=new String(uploadFileName.getBytes("GBK"),"ISO-8859-1");
        FTPFile[] ftpFiles = ftpClient.listFiles(filePath);
        if (ftpFiles!=null&&ftpFiles.length>0){
            log.info("传输路径存在同名文件");
            flag=true;
        }else {
            flag=false;
        }
        return flag;
    }

    //关闭流和FTP连接
    public void closeConnection(FTPClient ftpClient,InputStream inputStream){
        try {
            if (ftpClient!=null&&ftpClient.isConnected()){
                ftpClient.disconnect();
            }
            if (inputStream!=null){
                inputStream.close();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    //下载文件
    public void downloadFile(String name, HttpServletResponse response){
        OutputStream os=null;
        InputStream is=null;
        String downloadFilePath=ftpConfig.uploadPath+"/"+name;
        FTPClient ftpClient=getFTPConnection();
        try {
            if(!isExistFile(ftpClient,downloadFilePath)){
                throw new MainException.FTPDownloadException("下载目标文件不存在");
            }else {
                is=ftpClient.retrieveFileStream(downloadFilePath);
                os=response.getOutputStream();
                byte[] bytes=new byte[is.available()];
                int i;
                while ((i=is.read(bytes))!=-1){
                    os.write(bytes,0,i);
                }
                log.info("FTP文件输入流成功写入Response输出流");
            }
        }catch (IOException e) {
            log.error(e.getMessage(),e);
            throw new MainException.FTPDownloadException("FTP流操作失败");
        }finally {
            closeConnection(ftpClient,is);
        }
    }

}