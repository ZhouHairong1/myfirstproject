package com.Graduation.Dao.Mappers;

import com.Graduation.Pojo.Thesis;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ThesisDao {

    //判断是否存在对应名称的论文
    Thesis selectThesis(Thesis thesis);

    //插入论文
    int insertThesis(Thesis thesis);
}
