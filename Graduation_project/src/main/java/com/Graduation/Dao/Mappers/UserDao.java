package com.Graduation.Dao.Mappers;


import com.Graduation.Pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserDao {

    //查询单条User记录
    User getUser(User user);

    //注册用户
    int insertUser(User user);

    //通过用户名查询用户id
    int selectUserIdByName(String username);

    //获取用户总数
    int getUserNum();
}