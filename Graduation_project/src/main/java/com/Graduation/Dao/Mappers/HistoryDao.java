package com.Graduation.Dao.Mappers;

import java.util.List;

public interface HistoryDao {

    //获取某个用户的访问历史记录
    List<HistoryDao> getHistoryById(int userId);

}
