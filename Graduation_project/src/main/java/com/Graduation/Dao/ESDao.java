package com.Graduation.Dao;

import com.Graduation.Config.ESConfig;
import com.Graduation.Exception.MainException;
import com.Graduation.Pojo.ESDocument;
import com.Graduation.Pojo.Thesis;
import com.Graduation.Utils.Log;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.AnalyzeRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-01 21:44
 **/
@Repository

public class ESDao {

    private static final Logger log= Log.rootLogger;

    @Autowired
    ESConfig esConfig;

    @Autowired
    RestHighLevelClient client;

    @Autowired
    RestTemplate restTemplate;

    public void storeThesis(Thesis thesis) {
        IndexResponse indexResponse=null;
        try {
            IndexRequest request=new IndexRequest(esConfig.ES_INDEX);
            request.timeout("3s");
            request.source(JSON.toJSONString(thesis), XContentType.JSON);
            indexResponse =client.index(request, RequestOptions.DEFAULT);
            if (indexResponse.status().getStatus()>=400){
                throw new MainException.ESAddException();
            }
        }catch (Exception e) {
            throw new MainException.ESAddException();
        }
    }

    public Object searchThesis(String name)throws IOException {
        List<Map<String,Object>> maps =new ArrayList <>();
        SearchRequest searchRequest=new SearchRequest(esConfig.ES_INDEX);
        SearchSourceBuilder sourceBuilder=new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.matchQuery("title",name));
        sourceBuilder.query(QueryBuilders.matchQuery("content",name));
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse=client.search(searchRequest,RequestOptions.DEFAULT);
        for (SearchHit hit : searchResponse.getHits().getHits()) {
            maps.add(hit.getSourceAsMap());
        }
        System.out.println(maps.toString());
        return maps;
    }


    public Object searchThesis_smart(String word, int userId) {
        List words=ik_smart(word);
        return null;
    }

    public List<String> ik_smart(String word){
        List<String> words=new ArrayList<>();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        JSONObject object=new JSONObject();
        object.put("analyzer","ik_smart");
        object.put("text",word);
        String jsonString=object.toJSONString();
        HttpEntity requestEntity =new HttpEntity(jsonString,requestHeaders);
        String returnString=restTemplate.postForObject("http://192.168.81.83:9200/news/_analyze?pretty",requestEntity,String.class);
        JSONObject jsonObject=JSONObject.parseObject(returnString);
        JSONArray jsonArray=jsonObject.getJSONArray("tokens");
        for (int i = 0; i < jsonArray.size(); i++) {
            words.add(jsonArray.getJSONObject(i).getString("token"));
        }
        return words;
    }

    public static void InsertDocs(String docName) throws IOException {
        RestClientBuilder builder=null;
        builder= RestClient.builder(new HttpHost("192.168.81.83",9200));
        RestHighLevelClient client=new RestHighLevelClient(builder);
        BulkRequest bulkRequest=new BulkRequest();

        StringBuffer text=new StringBuffer();
        String line;
        BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(docName),"UTF-8"));
        while ((line=br.readLine())!=null){
            text.append(line);
        }
        Document doc= Jsoup.parse(text.toString());
        Elements elements=doc.getElementsByTag("doc");
        for (Element element : elements) {
//            System.out.println("docno:"+element.getElementsByTag("docno").html());
//            System.out.println("contenttitle:"+element.getElementsByTag("contenttitle").html());
//            System.out.println("content:"+element.getElementsByTag("content").html());
            ESDocument esDocument=new ESDocument();
            IndexRequest request=new IndexRequest("news");
            esDocument.setTitle(element.getElementsByTag("contenttitle").html());
            esDocument.setContent(element.getElementsByTag("content").html());
            request.source(JSON.toJSONString(esDocument),XContentType.JSON);
            bulkRequest.add(request);
        }
        BulkResponse response = client.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println(response.status().getStatus());
    }

    public static void searchDemo(){

    }

    public static void main(String[] args) throws IOException {
        List<String> docNames=new ArrayList<>();
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.010806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.020806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.030806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.040806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.050806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.060806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.070806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.080806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.110806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.120806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.130806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.140806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.150806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.160806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.170806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.180806.txt");
        docNames.add("C:\\Users\\MAXXZZ\\Desktop\\数据\\news.sohunews.210806.txt");
        for (String docName : docNames) {
            InsertDocs(docName);
        }
    }

}