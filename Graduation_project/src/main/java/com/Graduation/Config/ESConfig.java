package com.Graduation.Config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-03-29 22:23
 **/
@Configuration
public class ESConfig {

    @Value("${es.ES_INDEX}")
    public String ES_INDEX;

    @Bean
    public RestHighLevelClient client(){
        RestClientBuilder builder=null;
        builder= RestClient.builder(new HttpHost("192.168.81.83",9200));
        RestHighLevelClient client=new RestHighLevelClient(builder);
        return client;
    }

}