package com.Graduation.Config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-01 21:42
 **/
@Component
@Getter
public class FTPConfig {

    @Value("${ftp.uploadPath}")
    public String uploadPath;

    @Value("${ftp.host}")
    public String host;

    @Value("${ftp.port}")
    public int port;

    @Value("${ftp.username}")
    public String username;

    @Value("${ftp.password}")
    public String password;
}