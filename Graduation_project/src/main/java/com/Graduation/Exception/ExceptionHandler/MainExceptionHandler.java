package com.Graduation.Exception.ExceptionHandler;

import com.Graduation.Exception.MainException;
import com.Graduation.Pojo.Result;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-03-30 20:27
 **/
@RestControllerAdvice
public class MainExceptionHandler {
    public static final String FORWARDED_PROTO_HEADER_NAME = "x-forwarded-proto";
    public static final String FORWARDED_HOST_HEADER_NAME = "x-forwarded-host";

    @ResponseBody
    @ExceptionHandler(value = MainException.class)
    public ModelAndView requestExceptionHandler(HttpServletRequest request, MainException exception) {
        String requestURL = request.getRequestURL().toString();
        if (StringUtils.isNotBlank(request.getHeader(FORWARDED_PROTO_HEADER_NAME)) && StringUtils.isNotBlank(request.getHeader(FORWARDED_HOST_HEADER_NAME))) {
            requestURL = request.getHeader(FORWARDED_PROTO_HEADER_NAME) + "://" + request.getHeader(FORWARDED_HOST_HEADER_NAME) + request.getRequestURI();
        }
        String statusString = exception.getMessage();
        if (StringUtils.isNotBlank(statusString)) {
            try {
                statusString = URLDecoder.decode(statusString, "utf-8");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            statusString = exception.getMessage();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("result", new Result(exception.getDetail(), statusString, requestURL));
        modelAndView.setViewName("/fail.html");
        return modelAndView;
    }
}


