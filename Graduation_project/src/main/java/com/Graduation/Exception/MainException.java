package com.Graduation.Exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-03-30 20:24
 **/
public class MainException extends RuntimeException {

    private String detail;

    private String message;

    private int httpStatus;

    public MainException() {

    }

    public MainException(String message) {
        this.message = message;
        this.httpStatus = 500;
    }

    public MainException(String message, String detail) {
        this.message = message;
        this.detail = detail;
        this.httpStatus = 500;

    }

    public MainException(String message, String detail, int httpStatus) {
        this.detail = detail;
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public static class ESSearchException extends MainException {
        public ESSearchException() {
            super("执行ES查询操作失败", "ES错误", 500);
        }

        public ESSearchException(String message) {
            super("执行ES查询操作失败", message, 500);
        }

    }

    public static class ESAddException extends MainException {
        public ESAddException() {
            super("执行ES插入操作失败", "ES错误", 500);
        }

        public ESAddException(String message) {
            super("执行ES插入操作失败", message, 500);
        }
    }

    public static class DocReadException extends MainException {
        public DocReadException() {
            super("读取Doc文件失败", "Doc读取错误", 500);
        }

        public DocReadException(String message) {
            super("读取Doc文件失败", message, 500);
        }
    }

    public static class  UploadException extends MainException {
        public UploadException() {
            super("上传文件格式异常", "上船文件错误错误", 500);
        }
        public  UploadException(String message) {
            super("读上传文件格式异常", message, 500);
        }
    }

    public static class FTPUploadException extends MainException{
        public FTPUploadException() {
            super("FTP文件上传异常", "FTP错误", 500);;
        }

        public FTPUploadException(String message){
            super("FTP文件上传异常", message, 500);
        }
    }

    public static class FTPConnectionException extends MainException{
        public FTPConnectionException() {
            super("FTP连接失败", "FTP错误", 500);;
        }

        public FTPConnectionException(String message){
            super("FTP连接失败", message, 500);
        }

    }

    public static class FTPDownloadException extends MainException{
        public FTPDownloadException() {
            super("FTP文件下载失败", "FTP错误", 500);;
        }

        public FTPDownloadException(String message){
            super("FTP文件下载失败", message, 500);
        }
    }

    public static class FileTypeException extends MainException{
        public FileTypeException() {
            super("上传文件类型错误", "文件格式异常", 500);;
        }

        public FileTypeException(String message){
            super("上传文件类型错误", message, 500);
        }
    }

    public static class RedisSearchException extends MainException{
        public RedisSearchException() {
            super("Redis中不存在该文件注册信息", "Redis注册信息异常", 500);;
        }

        public RedisSearchException(String message){
            super("Redis注册信息异常", message, 500);
        }
    }

    public static class MysqlSearchException extends MainException{
        public MysqlSearchException() {
            super("Mysql查询信息异常", "Mysql查询信息异常", 500);;
        }

        public MysqlSearchException(String message){
            super("Mysql查询信息异常", message, 500);
        }
    }
}