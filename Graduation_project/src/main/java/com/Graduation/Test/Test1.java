package com.Graduation.Test;

import com.Graduation.Dao.Mappers.UserDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-03-30 23:04
 **/
@SpringBootTest
public class Test1 {

    @Autowired
    UserDao userDao;

    @Autowired
    RedisTemplate redisTemplate;


    @Test
    void testRedis(){
        System.out.println(redisTemplate.opsForValue().get("k1"));
    }
}