package com.Graduation;

import com.Graduation.Utils.Log;
import com.Graduation.common.ApplicationContextUtil;
import com.Graduation.common.TokenCleanTask;
import io.netty.util.concurrent.DefaultThreadFactory;
import org.slf4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App{

    private static final Logger log = Log.rootLogger;

    public static void main(String[] args) {
        ApplicationContext applicationContext=SpringApplication.run(App.class,args);
        new ApplicationContextUtil().setApplicationContext(applicationContext);
        //日志清除线程调度器
        ScheduledExecutorService scheduledExecutorService=new ScheduledThreadPoolExecutor(2,new DefaultThreadFactory("token清除"));
        scheduledExecutorService.scheduleAtFixedRate(new TokenCleanTask(),0L,1800000L, TimeUnit.MILLISECONDS);
        Environment environment= ApplicationContextUtil.getBean("environment",Environment.class);
        log.info("===========基于中文关键词的论文推荐系统启动成功(监听端口："+environment.getProperty("server.port")+")===========");
    }
}
