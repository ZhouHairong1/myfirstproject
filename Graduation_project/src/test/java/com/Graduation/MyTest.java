package com.Graduation;

import com.Graduation.Dao.ESDao;
import com.Graduation.Dao.FTPDao;
import com.Graduation.Utils.RSAUtil;
import com.Graduation.common.Constants;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: Graduation_project
 * 描述：
 * @author: 1289919603@qq.com
 * @create: 2021-04-03 10:53
 **/
@SpringBootTest
public class MyTest {

    @Autowired
    FTPDao ftpDao;

    @Autowired
    ESDao esDao;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;


    @Test
    void testFtp() throws IOException {
        FTPClient ftpClient=ftpDao.getFTPConnection();
        boolean exist=ftpDao.isExistFile(ftpClient,"/Thesis/测试.docx");
        if (exist){
            System.out.println("yes");
        }else {
            System.out.println("no");
        }
    }

    @Test
    void testEs() throws IOException {
        System.out.println(esDao.searchThesis("111"));
    }

    @Test
    void testRedis(){
        stringRedisTemplate.opsForValue().set("k1","中文");
        System.out.println(stringRedisTemplate.opsForValue().get("k1"));
    }

    @Test
    void poi1() throws IOException {
        HWPFDocument document=new HWPFDocument(new FileInputStream(new File("C:\\Users\\MAXXZZ\\Desktop\\毕设\\hello.doc")));
        System.out.println(document.getText());
    }

    @Test
    void poi2() throws IOException {
        XWPFDocument document=new XWPFDocument(new FileInputStream(new File("C:\\Users\\MAXXZZ\\Desktop\\毕设\\新建 DOCX 文档.docx")));
        XWPFWordExtractor wordExtractor=new XWPFWordExtractor(document);
        System.out.println(wordExtractor.getText());
    }

    public static void main(String[] args) throws Exception {
//        JSONObject object=new JSONObject();
//        object.put("username","zhangsan");
//        object.put("password","123456");
//        object.put("publicKey","MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoFUvBDpaU07LyEyF4q9rjPA/NI3O+qMq5wDAIT4m2miDyYztK8Bv5p9bIkQkJlROCA/4sR9DnmDlyz6ywIrOOR4KyNoA7HrBXes1iqObf8eIcKOawgVx0oWWgds+af2B9niz+RMgqryuTtR7yQEak8BxgAskajLyCjgMkOQAap0klvGlE8q8DfklHhooik3JCto8oNj0LvTa3QBQ4/TQ5sBzSHEKyhNtQ0lhFIMiPLNoqfLZkyIV8m0Cd8Gh3+RdCUfKCK+x+DCQEsZAJodrCQs5miExqgKn7I+N5uSPLwV2k7ZqsU42LrJX+i7bYri8dhP5afBQBIE+c7JBV+W/2QIDAQAB");
//        System.out.println(object.toJSONString());

        String publicKeyString="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlO4CjNINM6kGZzf2orVuPq+IU5SwifjHsNzGxkxzZEp0EWytjGOPkKbCKuGvD8ipAmRqvNI0hKY6NsaS8cijqBAr/Cas5q61VUnct9bAb+46tvAbYZUklIdkBPChe1Fg0dJUIFOLzNJ1dED1wBlnf/L8QLFC/MWb5wt6pWwypW9q+F1KtI55fgYzsNIuz3Mb3VzBDXHgcvaxwNzkDlCvyMivMXDkBfxWCJ36brKaQiDk5s+JkqExExJ5TZpfDZh489ay6JBjEX9NIrwzny0TmNRJwNdndKvguv+Lc9wMW6IlE1/KSJbm+Ap9zfOdOhiUcp1G0X8frrwHsbh+biGBdQIDAQAB";

        byte[] bytes="123456".getBytes();

        RSAPublicKey publicKey=RSAUtil.loadPublicKeyByStr(publicKeyString);
        byte[] result=RSAUtil.encrypt(publicKey,bytes);

        String afterEncode=new String(Base64.getEncoder().encode(result));

        System.out.println("加密后可视字符串："+afterEncode);


    }

    @Test
    public void makeJson(){
        JSONObject object=new JSONObject();
        object.put("username","zhangsan");
        object.put("token","98e3bd69107a9233b4659f853256497970493fa41ae2da99873eb15c53e5a256");
        System.out.println(object.toJSONString());
    }

    @Test
    public void getRedis(){
        System.out.println(String.valueOf(redisTemplate.opsForHash().get(Constants.AUTHENTICATE_TOKEN,"zhangsan")));
    }
}